# README #

### What is this repository for? ###
MATLAB implementation of the simultanious diagonalization algorithm (FFDiag) described in "A Fast Algorithm for Joint Diagonalization with Non-orthogonal Transformations and its Application to Blind Source
Separation" by Ziehe et al. (2004)

### How do I get set up? ###

Use the FFDiag function in FFDiag.m to simultaniously diagonalize matrices

INPUTS:
    
1. "C": matrix with dimensions matrix_count X variables x variables
   I.e. Joint aproximate diagonalization of three covariance matrices 
   from a 32 electrode EEG recording: 3x32x32 input matrix
     
2. "eps": Stop criterion. When change in norm of E (off-diagonal elements) is 
   below a certain threshold, the optimization stops (default is 1e-6)
   
3.     "max_iterations": Maximum allowed number of iterations before
       termination (default is 200)

### Who do I talk to? ###
Feel free to contact me through BitBucket if you have any questions or would like to contribute :-)