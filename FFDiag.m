function [ V ] = FFDiag( C, eps, max_iterations, verbose )
    % Implementation of the simultanious diagonalization algorithm (FFDiag) 
    % described in "A Fast Algorithm for Joint Diagonalization with
    % Non-orthogonal Transformations and its Application to Blind Source
    % Separation" by Ziehe et al. (2004)
    
    % ::::: INPUTS :::::
    
    % "C": matrix with dimensions matrix_count X variables x variables
    % I.e. Joint aproximate diagonalization of three covariance matrices 
    % from a 32 electrode EEG recording: 3x32x32 input matrix
    
    % "eps": Stop criterion. When change in norm of E (off-diagonal elements) is 
    % below a certain threshold, the optimization stops (default is 1e-6)
    
    % "max_iterations": Maximum allowed number of iterations before
    % termination (default is 200)

    if ndims(C) ~= 3
        error('Input matrix C do not have three dimensions as required');
    end
    
    if size(C,2) ~= size(C,3)
        error('Input matrices to be diagonalized have to be square');
    end
    
    % Get properties from input
    matrix_count = size(C,1);
    variables = size(C,2);
    
    % Options
    if nargin < 2
        eps = 1e-6;
    end
    if nargin < 3
        max_iterations = 200;
    end
    
    if nargin < 4
        verbose = 0;
    end
   theta = 0.95;

   % Initialize variables
   I = eye(variables); % Identity matrix
   n = 1; % Iteration counter
   V = I; % Projection matrix
   W = zeros(variables, variables); % Update matrix
   W_norm = Inf;
   E_norm = 0;
   E_last_norm = Inf;
   
   if verbose == 1
       W_norms = zeros(1,max_iterations);
       E_norms = zeros(1,max_iterations);
   end
   
   while abs(E_last_norm - E_norm) > eps && n <= max_iterations
       
      % Determine diagonal and off-diagonal parts of matrices
      D = zeros(matrix_count, variables);
      E = zeros(matrix_count, variables, variables);
      for m = 1:matrix_count
        D(m,:) = diag(squeeze(C(m,:,:)));
        E(m,:,:) = squeeze(C(m,:,:)) - diag(D(m,:));
      end
      
      % Compute W
      for i = 1:variables
          for j = 1:variables
              
              % Do not calculate for diagonal elements
              if i == j
                  continue;
              end
                
              % Compute W from covariance matrices according to equation 17
              Wp_num = (f_z(i,j,D)*f_y(j,i,D,E) - f_z(i,i,D)*f_y(i,j,D,E));
              Wp_denom = f_z(j,j,D)*f_z(i,i,D) - (f_z(i,j,D)^2);
              W(i,j) = Wp_num / Wp_denom;
          end
      end
      
      % Normalize W if too large to ensure invertibility of (I + W)
      W_norm = norm(W, 'fro');
      if W_norm > theta
          W = (theta/W_norm)*W;
      end
      
      % Update unmixing matrix in each iteration
      V = (I + W) * V;
      
      E_last_norm = E_norm; % Save last state to know when to stop
      
      E_norm = 0;
      % Update covariance matrices and calculate objective function value
      for m = 1:matrix_count
        C(m,:,:) = (I + W)*squeeze(C(m,:,:))*(I + W)';
        E_temp = squeeze(C(m,:,:)) - diag(diag(squeeze(C(m,:,:))));
        E_norm = E_norm + norm(E_temp, 'fro');
      end
      
      if verbose == 1
          E_norms(1,n) = E_norm;
          W_norms(1,n) = W_norm;
      end
      
      % Increase counter
      n = n + 1;
   end
   
   if verbose == 1
          figure('units','centimeters','position',[10 20 21 7]);
          subplot(1,2,1);
          hold on;
          plot(1:n-1,W_norms(1:n-1), '*b');
          title('Norm of W without scaling');
          grid on;
          axis([0 n 0 max(W_norms)+1]);
          xlabel('Iterations');
          ylabel('||W||_F');

          subplot(1,2,2);
          hold on;
          plot(1:n-1,E_norms(1:n-1), '*r');
          title('Norm of off-diagonal covariance entries');
          axis([0 n 0 max(E_norms)]);
          xlabel('Iterations');
          grid on;
          ylabel('||E||_F');
          fprintf('W-norm: %0.2e \n', W_norm);
          fprintf('E-norm: %0.2e \n', E_norm);
          fprintf('%d iterations\n', n);
   end
end

function [ out ] = f_y( i, j, D, E )
    out = sum(D(:,j).*E(:,i,j));
end

function [ out ] = f_z( i, j, D )
    out = sum(D(:,i).*D(:,j));
end